# SellUP Python Library

This library is a public data-manipulation toolset, indended to ease the process of extracting meaningful conclusions from data.

Most if not all accessible functions defined in this library are extensions to the Pandas library.

## Installation and Use:

Install using the following command:

`pip install git+https://git@bitbucket.org/sellupinc/sellup_python.git@master#egg=sellup`

Access with the following:

```python
import sellup as sup

print(sup.this_week)

>> DatetimeIndex(['2019-10-13', '2019-10-14', '2019-10-15', 
                  '2019-10-16', '2019-10-17', '2019-10-18', 
                  '2019-10-19'], dtype='datetime64[ns]', freq='D')
```