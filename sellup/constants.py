from dateutil.parser import parse
import datetime as dt
import pandas as pd

__all__ = [
    'Dates',
    'date_from_iso',
    'sun',
    'sat',
    'this_week',
    'last_week',
    'prev_6w',
    'sun_ly',
    'sat_ly',
    'this_week_ly',
    'last_week_ly',
    'prev_6w_ly',
    'rate_headers',
    'headers',
    'format_dict',
    'column_format',
    'default_export_range'
]

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#|||||||||||||||||||  D A T E  D E F I N I T I O N  |||||||||||||||||||||||||||
#//////////////////////////////////////////////////////////////////////////////

def date_from_iso(iso_year, iso_week, iso_day):
    """
    Return Gregorian values, given ISO date objects.
    """

    def _iso_year_start(iso_year):
        """
        Determine the start of the year, given the year of interest.
        """

        fourth_jan = dt.date(iso_year, 1, 4)
        delta = dt.timedelta(fourth_jan.isoweekday() - 1)
        return fourth_jan - delta
    year_start = _iso_year_start(iso_year)
    return year_start + dt.timedelta(days=iso_day - 1, weeks=iso_week - 1)

class Dates:

    def _satandsun(self,date=None):
        """
        Determine the most recently passed Sun/Sat pair for a given date.
        """
        
        date = dt.datetime.now() if date is None else date
        og_ordinal    = date.toordinal()
        last_week     = og_ordinal - 7
        last_saturday = last_week - (last_week % 7) + 6
        last_sunday   = last_saturday - 6

        last_sunday   = dt.date.fromordinal(last_sunday)
        last_saturday = dt.date.fromordinal(last_saturday)
        return last_sunday, last_saturday

    def __init__(self,date=None,year=None,week=None,day=None):
        """
        Define all important reference dates, provided a root.
        """

        if all([date,any([year,week,day])]):
            raise ValueError('Please define a date by date-string, or year/month/day')
        elif any([year,week,day]):
            y,w,d = dt.datetime.now().isocalendar()
            y = year if year else y
            w = week if week else w
            d = day if day else d
            self.sun, self.sat = self._satandsun(date_from_iso(y,w,d))
        elif date:
            if isinstance(date,str):
                self.sun, self.sat = self._satandsun(parse(date))
            else:
                self.sun, self.sat = self._satandsun(date)
        else:
            self.sun, self.sat = self._satandsun()

        self.this_week = pd.date_range(self.sun, self.sat)
        self.last_week = pd.date_range(self.sun - dt.timedelta(weeks=1),
                                       self.sat - dt.timedelta(weeks=1))
        self.prev_6w   = pd.date_range(self.sun - dt.timedelta(weeks=6),
                                       self.sat - dt.timedelta(weeks=1))

        self.sun_ly = date_from_iso(self.sun.year-1, self.sun.isocalendar()[1], 7)
        self.sat_ly = date_from_iso(self.sat.year-1, self.sat.isocalendar()[1], 6)
        self.this_week_ly = pd.date_range(self.sun_ly, self.sat_ly)
        self.last_week_ly = pd.date_range(self.sun_ly - dt.timedelta(weeks=1),
                                          self.sat_ly - dt.timedelta(weeks=1))
        self.prev_6w_ly   = pd.date_range(self.sun_ly - dt.timedelta(weeks=6),
                                          self.sat_ly - dt.timedelta(weeks=1))
    def set_date(self,date):
        self.__init__(date)
    def reset_dates(self):
        self.__init__()

_default_dates = Dates()
sun = _default_dates.sun
sat = _default_dates.sat
this_week = _default_dates.this_week
last_week = _default_dates.last_week
prev_6w   = _default_dates.prev_6w

sun_ly = _default_dates.sun_ly
sat_ly = _default_dates.sat_ly
this_week_ly = _default_dates.this_week_ly
last_week_ly = _default_dates.last_week_ly
prev_6w_ly   = _default_dates.prev_6w_ly

default_export_range = pd.date_range(min(prev_6w),max(this_week))
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#|||||||||||||||||||  S T D  D E F I N I T I O N S  |||||||||||||||||||||||||||
#//////////////////////////////////////////////////////////////////////////////

rate_headers = {
    'Opens / Delivered'   : ('Opens',  'Delivered'),
    'Clicks / Delivered'  : ('Clicks', 'Delivered'),
    'Clicks / Open'       : ('Clicks', 'Opens'),
    'AOV'                 : ('Revenue','Orders'),
    'Conversion'          : ('Orders', 'Clicks'),
    'Unsub / Delivered'   : ('Unsub', 'Delivered'),
    'eCPM'                : ('Revenue','Delivered'),
    'GA AOV'              : ('GA Revenue','GA Orders'),
    'GA Conversion'       : ('GA Orders', 'Clicks'),
    'GA Orders / Session' : ('GA Orders', 'GA Sessions'),
    'GA eCPM'             : ('GA Revenue','Delivered'),
}

headers = {
    'all' : [
        'Delivered',
        'Opens',
        'Opens / Delivered',
        'Clicks',
        'Clicks / Delivered',
        'Clicks / Open',
        'Revenue',
        'Orders',
        'AOV',
        'Conversion',
        'Unsub',
        'Unsub / Delivered',
        'eCPM',
        'GA Revenue',
        'GA Orders',
        'GA Sessions',
        'GA AOV',
        'GA Conversion',
        'GA Orders / Session',
        'GA eCPM'
    ],
    'esp' : [
        'Delivered',
        'Opens',
        'Opens / Delivered',
        'Clicks',
        'Clicks / Delivered',
        'Clicks / Open',
        'Revenue',
        'Orders',
        'AOV',
        'Conversion',
        'Unsub',
        'Unsub / Delivered',
        'eCPM',
    ],
    'ga' : [
        'Delivered',
        'Opens',
        'Opens / Delivered',
        'Clicks',
        'Clicks / Delivered',
        'Clicks / Open',
        'GA Revenue',
        'GA Orders',
        'GA Sessions',
        'GA AOV',
        'GA Conversion',
        'GA Orders / Session',
        'Unsub',
        'Unsub / Delivered',
        'GA eCPM'
    ],
    'op' : [
        'Delivered',
        'Opens',
        'Clicks',
        'Revenue',
        'Orders',
        'Unsub',
        'GA Revenue',
        'GA Orders',
        'GA Sessions',
    ]
}

format_dict = {
    'number' : '{:,.0f}',
    'percent': '{:,.2%}',
    'dollar' : '${:,.0f}',
    'euro'   : '€{:,.0f}'
}

column_format = {
    'Sent'                : format_dict['number'],
    'Count'               : format_dict['number'],
    'Delivered'           : format_dict['number'],
    'Opens'               : format_dict['number'],
    'Opens / Delivered'   : format_dict['percent'],
    'Clicks'              : format_dict['number'],
    'Clicks / Delivered'  : format_dict['percent'],
    'Clicks / Open'       : format_dict['percent'],
    'Revenue'             : format_dict['dollar'],
    'Orders'              : format_dict['number'],
    'AOV'                 : format_dict['dollar'],
    'Conversion'          : format_dict['percent'],
    'Unsub'               : format_dict['number'],
    'Unsub / Delivered'   : format_dict['percent'],
    'eCPM'                : format_dict['dollar'],
    'GA Revenue'          : format_dict['dollar'],
    'GA Orders'           : format_dict['number'],
    'GA Sessions'         : format_dict['number'],
    'GA AOV'              : format_dict['dollar'],
    'GA Conversion'       : format_dict['percent'],
    'GA Orders / Session' : format_dict['percent'],
    'GA eCPM'             : format_dict['dollar'],
    'number'              : format_dict['number'],
    'dollar'              : format_dict['dollar'],
    'percent'             : format_dict['percent'],
    'euro'                : format_dict['euro']
}