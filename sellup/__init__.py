from sellup.constants import *
from sellup.plotting import *
from sellup.data_manipulation import *

#module level docstring
__doc__ = """
sellup_pydata is a bespoke python library for data analysis and reporting
at SellUP NYC.

Its operation is built primarily on top of the pandas dataframe library.
It is required that the primary df used for manipulation has a datetimeindex.
"""