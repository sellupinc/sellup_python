import matplotlib.pyplot as plt
import sellup.constants as const
import numpy as np

__all__ = [
    'format_axis',
    'plot_sheet'
]

def format_axis(ax,form,axis='y',limit_decimals=True):
    if axis.lower() == 'y':
        ticks = ax.get_yticks()
        set_ticks = lambda y: ax.set_yticklabels(y)
    elif axis.lower() == 'x':
        ticks = ax.get_xticks()
        set_ticks = lambda x: ax.set_xticklabels(x)
    else:
        raise ValueError("Only 2-D plots are supported. Use 'x' or 'y' axis.")

    ticklabels = [const.column_format[form].format(x if 1 > x > 0 else int(x)) for x in ticks]
    set_ticks(ticklabels)

def plot_sheet(df,defaults=True,specifics={},nrows=0,ncols=0,figsize=(),hspace=0.35,**kwargs):
    """
    Provided a hierarchical dataframe, a series of comparative plots are
    stitched into a single image.

    Titles are copied from the column header unless otherwise specified
    via specifics or **kwargs.

    Plots are placed in the order which they are specified in the df.

    kwargs are passed through to pyplot.

    Params
    ------
    df        : pandas dataframe with conventional SellUP headers in
                hierarchical index.
    defaults  : bool, accept of the default settings for weekly plots.
                (They automatically join with any extra user-kwargs,
                giving priority to user-kwargs.)
    specifics : Should different params be needed for specific plots,
                organize them inside a dictionary as follows:

                specifics = {
                    'column_name' : {
                        'color' : 'red',
                        'title' : 'Example'
                    }
                }
    """

    top_most_cols = df.columns.get_level_values(0).unique()

    if any([nrows,ncols]):
        if all([nrows,ncols]):
            pass
        elif ncols:
            nrows = int(np.ceil(ncols / len(top_most_cols)))
        elif nrows:
            ncols = int(np.ceil(nrows / len(top_most_cols)))
    else:
        if len(top_most_cols) < 4:
            nrows = 1
        elif len(top_most_cols) < 7:
            nrows = 2
        elif len(top_most_cols) < 13:
            nrows = 3
        else:
            raise ValueError('Limit table top-level headers to 12 plots or' \
                            +' define your own layout using nrows, ncols.')
        ncols = int(np.ceil(len(top_most_cols)/nrows))
    figsize = (8*ncols,4*nrows) if not figsize else figsize
    
    # Initialize plot_sheet
    fig, axes = plt.subplots(nrows,ncols,figsize=figsize)
    plt.subplots_adjust(hspace=hspace)
    x,y = 0,0

    # Plot column data.
    for i,col in enumerate(top_most_cols):
        ax=axes[y,x] if nrows > 1 else axes[x]
    
        # Remove delicate kwargs from dicts which are passed
        # to external, dependent functions.
        tick_format = specifics[col].pop('tick_format',col) \
                      if col in specifics else col

        # Define Default settings.
        default_kwargs = {
            'title'     : col,
            'grid'      : True,
            'legend'    : False,
            'linewidth' : 2
        }

        if defaults:
            approved_kwargs = {**kwargs,
                               **{k:v for k,v in default_kwargs.items() \
                                  if k not in kwargs}}
        else:
            approved_kwargs = kwargs
        
        # Handle specific instructions.
        if col in specifics:
            if defaults:
                approved_specs = {**specifics[col],
                                  **{k:v for k,v in default_kwargs.items() \
                                     if k not in specifics[col]}}
            else:
                approved_specs = specifics[col]
            ax = approved_specs.pop('ax',ax)
            df[col].plot(ax=ax,**approved_specs)
        else:
            df[col].plot(ax=ax,**approved_kwargs)
            
        if defaults:
            ax.title.set_fontsize(18)
            if kwargs.get('kind',False) == 'barh':
                try:
                    format_axis(ax,tick_format,axis='x')
                except:
                    format_axis(ax,'number',axis='x')
            else:
                try:
                    format_axis(ax,tick_format)
                except:
                    format_axis(ax,'number')

        # Handle the coordinate locators for the next plot.
        if x < ncols-1:
            x+=1
        else:
            x=0
            y+=1

    if defaults:
        first_subplot = axes[0,0] if nrows > 1 else axes[0]
        handles, labels = first_subplot.get_legend_handles_labels()
        if kwargs.get('legend',True):
            fig.legend(handles, labels, loc='lower center', ncol=len(labels))

    # If there are too many subplots, delete the extras.
    extra_cells = ((nrows*ncols) - (i + 1))
    if extra_cells:
        x = ncols-1
        y = nrows-1
        
        for _ in range(extra_cells):
            ax = axes[y,x]
            ax.set_axis_off()
            
            # Handle the coordinate locators for the next plot.
            if x > 0:
                x-=1
            else:
                x=ncols-1
                y-=1
    return fig