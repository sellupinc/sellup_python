import sellup.constants as const
from dateutil.parser import parse
import calendar
import pandas as pd
import numpy as np
import datetime as dt

__all__ = [
    'date_filter',
    'ratify',
    'fillna_body',
    'pivot',
    'pct_diff',
    'window',
    'form',
    'comp'
]

def date_filter(df,*args,start=None,end=None,year=None,weeks=None,week=None,date_range=None):
    """
    Filter a given df by date.
    """

    if args:
        assert len(args) < 3, ValueError('Too many arguments given. Provide [(date_range),(start,end),(year,week/s)]')
        if len(args) == 1:
            assert isinstance(args[0],pd.DatetimeIndex), ValueError('If providing one argument, pass it through as pd.date_range/pd.DatetimeIndex.')
            date_range = args[0]
        else:
            if not [x for x in args if isinstance(x,int)]:
                start = args[0]
                end = args[1]
            else:
                year = [x for x in args if x > 1900][0]
                weeks = [x for x in args if x != year]

    if all([start, end]):
        start = parse(start) if isinstance(start, str) else start
        end   = parse(end) if isinstance(end, str) else end
        if start > end:
            start, end = end, start
        start = pd.to_datetime(start)
        end = pd.to_datetime(end)
    elif all([year, any([week,weeks])]):
        assert not all([week,weeks]), ValueError('Please provide only one week descriptor.')
        weeks = week if week else weeks
        weeks = list(weeks)
        start = const.date_from_iso(year, min(weeks),0)
        end   = const.date_from_iso(year, max(weeks),6)
        start = pd.to_datetime(start)
        end   = pd.to_datetime(end)
    elif date_range is not None:
        start = date_range.min()
        end   = date_range.max()
    else:
        raise ValueError('Provide clearer date-range information.')
    return df[(df.index >= start) & (df.index <= end)]

def ratify(df):
    """
    Create the rate columns for a given table.
    
    Column headers not involved in calculations will be ignored.
    
    Params
    ------
    df : pandas dataframe with conventional SellUP headers.
    """

    header_height = len(df.columns.names) - 1
    for _ in range(header_height):
        df = df.stack()
    for col,comps in const.rate_headers.items():
        try:
            # ecpm is a rate multiplied by 1000.
            ecpm_factor = 1000 if 'ecpm' in col.lower() else 1
            df[col] = ecpm_factor*( df[comps[0]].astype(float) \
                                  / df[comps[1]].astype(float) )
        except:
            pass
    for _ in range(header_height):
        df = df.unstack()
    return df

def fillna_body(df,unstacked=False):
    """
    dfs which contain holes that need to be filled, but retain absent
    data for leading and future terms.
    
    Params
    ------
    df : pandas dataframe with conventional SellUP headers.
    """
    
    if unstacked:
        df_res = pd.DataFrame()
        for col in df.columns.get_level_values(0).unique():
            df_slice = fillna_body( df[col].copy() )
            df_slice.columns = pd.MultiIndex.from_product(
                               [[col], df_slice.columns],
                               names=['Metric', 'Year'])
            df_res = pd.concat([df_res, df_slice], axis=1)
        return df_res
    bfill_null = df.bfill().isnull()
    ffill_null = df.ffill().isnull()
    df[~( bfill_null | ffill_null )] = df.fillna(0)

    # Week 53 is likely not skipped by data omission. Undo.
    if '53' in df.index:
        df.loc['53'].replace(0, np.nan, inplace=True)
        
    return df

def pivot(df, by='week', short_months=False):
    """
    Pivot an entire client's data by week, month, or quarter.
    For year-over-year analysis.

    The weekly grouping in this function is fitted to match other results in this library.
    That is to say, weeks start/end on Sun/Sat, resp., regardless of real year.
    When pivoted by 'week', a year is defined by a "week-year".
    Refer to STRF iso-week-year definitions. 
    (%G and %V, when dates are shifted one day forward 
    to account for the Monday/Sunday start settings.) 

    Params
    ------
    df : pandas dataframe with conventional SellUP headers.
    """

    by = by.lower()[0]
    if by == 'w': # Week
        index_name = 'Week Number'
        index = (df.index + dt.timedelta(days=1)).strftime('%V')
        cols = (df.index + dt.timedelta(days=1)).strftime('%G')
        re_id = [str(i).zfill(2) for i in range(54)]
    elif by == 'm': # Month
        index_name = 'Month'
        strf = '%b' if short_months else '%B'
        index = df.index.strftime(strf)
        cols = df.index.year
        re_id = [m[:3] for m in calendar.month_name][1:] if short_months \
                else [m for m in calendar.month_name][1:]
    elif by == 'q': # Quarter
        index_name = 'Quarter'
        index = df.index.to_period('Q').strftime('Q%q')
        cols = df.index.year
        re_id = ['Q1','Q2','Q3','Q4']
    else:
        raise ValueError('Select "by" as either ["week","month","quarter"]')
    df_pivot = pd.pivot_table(df, index=index, columns=cols, aggfunc=sum)
    df_pivot.columns.rename(['Metric','Year'], inplace=True)
    df_pivot.index.rename(index_name, inplace=True)
    df_pivot = df_pivot.reindex(re_id)
    df_pivot = df_pivot.pipe(ratify)
    df_pivot = df_pivot.pipe(fillna_body, unstacked=True)
    return df_pivot

def pct_diff(df, index='% Diff', append=True):
    """
    Calculate the %-difference between the bottom two rows in any df.
    Typically used after some data manipulation for comparison.
    
    Infinity is replaced with 0.
    
    Params
    ------
    df pandas dataframe
        with conventional SellUP headers.
    index : string,
        index label for the %-diff row.
    append : boolean,
        attach to the bottom of the df?
    """

    index = pd.Index([index]) if isinstance(index, str) else index
    df_index_width = len(df.index.names)
    # Check to see if current index and planned index are the same size.
    # If this is the case, and lines are to be appended, fix it.
    if (append & ( df_index_width != len(index.names) )):
        index = pd.MultiIndex(
                levels = [['']]*(df_index_width-1) + [index],
                codes  = [[0]]*df_index_width,
                names  = df.index.names)
    res = df.pct_change().iloc[-1].to_frame().T
    res = res.replace((np.inf, -np.inf), 0).fillna(0)
    res.index = index
    if append:
        res = pd.concat([df,res])
    return res

def window(df,week_radius=6,years_incl=2,start=None,end=None,date_range=None,date=None):
    """
    Return a year over year, weekly comparison within a specific range. 
    All data inclusive of the weeks touched upon by the range defined.
    """

    date = dt.datetime.now() if date == None else date
    if years_incl < 1:
        return pd.DataFrame(columns=df.columns)

    if all([start, end]):
        start = parse(start) if isinstance(start,str) else start
        end   = parse(end) if isinstance(end,str) else end
    elif date_range is not None:
        start = date_range.min()
        end   = date_range.max()
    else:
        start = const.Dates(date).sun - dt.timedelta(weeks=week_radius - 1)
        end   = const.Dates(date).sat + dt.timedelta(weeks=week_radius)
    start_wk = start.isocalendar()[1]
    end_wk = end.isocalendar()[1]

    df_res = pd.DataFrame()
    for i in range(years_incl):
        window_start = const.date_from_iso(start.year-i, start_wk, 0)
        window_end   = const.date_from_iso(end.year-i,   end_wk,   6)
        window_range = pd.date_range(window_start, window_end)
        df_window = date_filter(df, date_range = window_range)
        df_window = df_window.groupby(df_window.index.to_period('W-SAT').week).sum()
        if start.year - i == start.year:
            week_index = window_range.to_period('W-SAT').week.unique()
            df_window = df_window.pipe(fillna_body)
        else:
            df_window = df_window.fillna(0)
        df_window = df_window.reindex(week_index,fill_value=np.nan)
        df_window['Year'] = start.year - i
        df_res = pd.concat([df_res,df_window])

    df_res = df_res.pivot(columns='Year').reindex(week_index)
    df_res.index.rename('Week Number',inplace=True)

    return df_res

def form(df,row_spec={},col_spec={},default='number',auto_pct=True):
    """
    Provide standard formatting to a given df.

    Params
    ------
    df            : pandas dataframe with conventional SellUP headers.
    row_spec      : dict, format type row index for rows which must be handled
                    differently than the rest of the columns they cross.
    col_spec      : dict, string format conditioning for specific columns.
    default       : provide safety net to format as this type.
    """

    header_height = len(df.columns.names) - 1
    for _ in range(header_height):
        df = df.stack()
    df = df.fillna(0)
    res = pd.DataFrame()
    for col in df.columns:
        if col in col_spec:
            try:
                form = col_spec[col]
            except:
                raise ValueError('Format string is in the wrong format.')
        else:
            try:
                form = const.column_format[col]
            except:
                try:
                    form = const.column_format[default]
                except:
                    raise ValueError('Default must be in sup.column_format')
        res[col] = df[col].apply(form.format)
    if row_spec:
        for k,v in row_spec.items():
            # If the key is an explicit format string, use it.
            if all(x in k for x in ['{','}']):
                form = k
            # Else, consider it the name of a default format option.
            else:
                form = const.column_format[k]
            if isinstance(v,list):
                for i in v:
                    res.iloc[i] = df.iloc[i].apply(form.format)
            else:
                res.iloc[v] = df.iloc[v].apply(form.format)
    for _ in range(header_height):
        res = res.unstack()
    if auto_pct:
        # Check the right-most index column for the term 'Diff'. Apply to all.
        r_most_index = df.index.get_level_values(len(df.index.names) - 1)
        if r_most_index.dtype == 'object':
            pct_diff_rows = df[r_most_index.str.lower().str.contains('diff',na=False)].squeeze()
            if len(pct_diff_rows) > 0:
                res.iloc[-1] = pct_diff_rows.apply(const.column_format['percent'].format)
    return res

def comp(df, kind='', params={}, date=None, incl_pct_diff=True):
    """
    Creates Comparison Data Table.
    YTD and MTD comparisons default to complete periods when the option is present.
    params:
    -------
    df: DataFrame
    kind: str, ['ytd','mtd','wow','yoy']
    params: settings to pass through. Example:
    {
        'groupby': 'Y',
        'filter': (year_condition & (month_condition | (latest_month_condition & date_condition))),
        'title': 'YTD',
        'pct_diff': bool,
        'row1': {
            'name': 2018,
            'range': 2018,
            'agg':'sum'
        },
        'row2': {
            'name': 2019,
            'range': 2019,
            'agg':'sum',
            'agg2':'mean'
        }
    }
    """
    
    date = dt.datetime.now() if date == None else date

    kind = kind.lower().replace('/','o')
    default_tables = ['ytd','mtd','wow','yoy']
    if kind in default_tables:
        d = const.Dates(date)
        year = d.this_week.to_period('Y').min().year
        month = d.this_week.to_period('M').min().month
        day = d.this_week.to_period('D').max().day
        if kind == 'ytd':
            primary_range = d.this_week.to_period('Y').min()
            year_condition = df.index.year.isin([year-1,year])
            month_condition = (df.index.month < month)
            latest_month_condition = (df.index.month == month)
            date_condition = (df.index.day <= day)
            default_params = {
                'groupby': 'Y',
                'filter': (year_condition & (month_condition | (latest_month_condition & date_condition))),
                'title': 'YTD',
                'pct_diff': incl_pct_diff,
                'row1': {
                    'name': primary_range - 1,
                    'range': primary_range - 1,
                    'agg':'sum'
                },
                'row2': {
                    'name': primary_range,
                    'range': primary_range,
                    'agg':'sum'
                }
            }
        elif kind == 'mtd':
            primary_range = d.this_week.to_period('M').min()
            year_condition = df.index.year.isin([year-1,year])
            month_condition = (df.index.month == month)
            date_condition = (df.index.day <= day)
            default_params = {
                'groupby': 'M',
                'title': 'MTD',
                'filter': (year_condition & month_condition & date_condition),
                'pct_diff': incl_pct_diff,
                'row1': {
                    'name':(primary_range - 12).year,
                    'range': primary_range - 12,
                    'agg':'sum'
                },
                'row2': {
                    'name':primary_range.year,
                    'range': primary_range,
                    'agg':'sum'
                }
            }
        elif kind == 'wow':
            week_condition = df.index.isin(d.this_week.append(d.prev_6w))
            last_week_sun = d.last_week.min().strftime('%-m/%-d')
            last_week_sat = d.last_week.max().strftime('%-m/%-d')
            this_week_sun = d.this_week.min().strftime('%-m/%-d')
            this_week_sat = d.this_week.max().strftime('%-m/%-d')
            default_params = {
                'groupby': 'W-SAT',
                'title': 'W / W',
                'filter': week_condition,
                'pct_diff': incl_pct_diff,
                'row1': {
                    'name': '-'.join([last_week_sun,last_week_sat]),
                    'range': d.last_week.to_period('W-SAT').min(),
                    'agg':'sum'
                },
                'row2': {
                    'name':'6W Mean',
                    'range': d.prev_6w.to_period('W-SAT').unique(),
                    'agg1':'sum',
                    'agg2':'mean'
                },
                'row3': {
                    'name': '-'.join([this_week_sun,this_week_sat]),
                    'range': d.this_week.to_period('W-SAT').min(),
                    'agg':'sum'
                }
            }
        elif kind == 'yoy':
            week_condition = df.index.isin(d.this_week.append(d.this_week_ly))
            default_params = {
                'groupby': 'W-SAT',
                'title': 'Y / Y',
                'filter': week_condition,
                'pct_diff': incl_pct_diff,
                'row1': {
                    'name': d.this_week_ly.to_period('W-SAT').year.min(),
                    'range': d.this_week_ly.to_period('W-SAT').min(),
                    'agg':'sum'
                },
                'row2': {
                    'name': d.this_week.to_period('W-SAT').year.min(),
                    'range': d.this_week.to_period('W-SAT').min(),
                    'agg':'sum'
                }
            }
    else:
        default_params = {}
    params = {**{k:v for k, v in default_params.items() if k not in params}, **params}
    if not params:
            raise ValueError("Pass through parameters or a table kind (['ytd','mtd','wow','yoy']).")
    default_params = {
        'pct_diff': True,
        'filter': None,
        'title': None,
        'groupby': 'W-SAT'
    }
    for k,v in default_params.items():
        if k not in params:
            params[k] = v
    df_trim = df[params['filter']]
    df_result = pd.DataFrame()

    rows = sorted([r for r in params if 'row' in r])
    for row in rows:
        row_params = params[row]
        aggs = sorted([a for a in row_params if 'agg' in a])
        for i, agg in enumerate(aggs):
            if i < 1:
                df_temp = df_trim.groupby(df_trim.index.to_period(params['groupby']))
            df_temp = df_temp.agg(row_params[agg])
            if i < 1:
                df_temp = df_temp.loc[row_params['range']]
        df_temp = df_temp.to_frame(row_params['name']).T
        df_result = pd.concat([df_result, df_temp])
    df_result.pipe(ratify)
    if params['pct_diff']:
        df_result = df_result.pipe(pct_diff)
    df_result.index.name = params['title']
    return df_result