import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="sellup",
    version="0.1.0",
    author="Walter Zielenski",
    author_email="walter.zielenski@sellup.net",
    description="A Python Library for SellUP Inc internal, standardized use.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="git@github.com:sellupinc/sellup_python.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: Linux",
    ],
    include_package_data=True,
    python_requires='>=3',
    install_requires=[
        'pandas>=0.25.2',
        'matplotlib>=3.0.3',
        'numpy>=1.16.5',
        "python-dateutil>=2.6.1"
    ],
)